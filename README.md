generate from proto file

protoc -I=. --micro_out=. --go_out=. proto/consignment/consignment.proto


docker build and run 

docker build -t shippy-service-consignment .
docker run -p 50051:50051 \
      -e MICRO_SERVER_ADDRESS=:50051 \
      shippy-service-consignment