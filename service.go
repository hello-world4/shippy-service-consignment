package main

import (
	"context"
	"log"
	"errors"
	pb "gitlab.com/hello-world4/shippy-service-consignment/proto/consignment"
	vesselProto "gitlab.com/hello-world4/shippy-service-vessel/proto/vessel"
)

type consignmentService struct {
	repo repository
	vesselService vesselProto.VesselService
}

func (s *consignmentService) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {
	log.Println("CreateConsignment")

	vesselResponse, err := s.vesselService.FindAvailable(ctx, &vesselProto.Specification{
		Capacity: int64(len(req.Containers)), 
		MaxWeight: int64(req.Weight),
	})

	if vesselResponse == nil {
		return errors.New("error fetching vessel, returned nil")
	}

	if err != nil {
		return err
	}

	req.VesselId = vesselResponse.Vessel.Id

	if err = s.repo.Create(ctx, MarshalConsignment(req)); err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req
	return nil
}

func (s *consignmentService) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	log.Println("GetConsignments")

	consignments, err := s.repo.GetAll(ctx)
	if err != nil {
		return err
	}
	res.Consignments = UnmarshalConsignmentCollection(consignments)
	return nil
}