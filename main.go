package main

import (
	"context"
	"errors"
	"log"
	"os"

	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/metadata"
	"github.com/micro/go-micro/v2/server"
	pb "gitlab.com/hello-world4/shippy-service-consignment/proto/consignment"
	userService "gitlab.com/hello-world4/shippy-service-user/proto/user"
	vesselProto "gitlab.com/hello-world4/shippy-service-vessel/proto/vessel"

	"github.com/micro/go-micro/v2/client"
)

const (
	defaultHost = "datastore:27017"
)

func main() {

	service := micro.NewService(
		micro.Name("shippy.service.consignment"),
		micro.WrapHandler(authWrapper),
	)

	service.Init()

	uri := os.Getenv("DB_HOST")
	if uri == "" {
		uri = defaultHost
	}

	dbClient, err := CreateClient(context.Background(), uri, 0)
	if err != nil {
		log.Panic(err)
	}
	defer dbClient.Disconnect(context.Background())

	consignmentCollection := dbClient.Database("shippy").Collection("consignments")
	repository := &MongoRepository{consignmentCollection}

	vesselService := vesselProto.NewVesselService("shippy.service.vessel", service.Client())

	if err := pb.RegisterShippingServiceHandler(
		service.Server(),
		&consignmentService{repository, vesselService},
	); err != nil {
		log.Panic(err)
	}

	if err := service.Run(); err != nil {
		log.Panic(err)
	}
}

func authWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, rsp interface{}) error {
		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}
	
		token := meta["Token"]

		authClient := userService.NewUserService(
			"shippy.service.user",
			client.DefaultClient,
		)
		authResp, err := authClient.ValidateToken(ctx, &userService.Token{
			Token: token,
		})
		log.Println("auth response ", authResp)
		log.Println("err: ", err)
		if err != nil {
			return err
		}

		err = fn(ctx, req, rsp)
		return err
	}
}
